extends KinematicBody2D

onready var projectile_scene = preload('res://Projectile.tscn')
onready var animation_node = $Animation
onready var reload_timeout = $ReloadTimeout
onready var dash_timeout = $DashTimeout
onready var dash_reload_timeout = $DashReload

const WALK_SPEED = 275
const DASH_SPEED = 900
const DASH_TIME_CONSTANT = 0.2
const MOUSE_SCALE = 2

export (int) var speed = WALK_SPEED

var reloading = false
var dash_ready = true
var dashing = false
var dash_dir = Vector2()
var move_dir = Vector2()
var velocity = Vector2()
var target = Vector2()
var sprite_dir = "down"

func _ready():
	pass

func get_movement_direction():
	move_dir = Vector2()
	if Input.is_action_pressed('move_right'):
		move_dir.x += 1
	if Input.is_action_pressed('move_left'):
		move_dir.x -= 1
	if Input.is_action_pressed('move_down'):
		move_dir.y += 1
	if Input.is_action_pressed('move_up'):
		move_dir.y -= 1

func handle_input():
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and not dashing:
		target = get_global_mouse_position()
		fire()
	if Input.is_action_just_pressed("move_dash") and move_dir != Vector2(0,0) and dash_ready:
		dash()

func _physics_process(delta):
	get_movement_direction()
	handle_input()
	
	get_sprite_direction()
	if move_dir != Vector2(0,0):
		play_animation("walk")
	else:
		play_animation("idle")
	
	if dashing:
		speed = WALK_SPEED + (DASH_SPEED - WALK_SPEED)*exp((dash_timeout.get_time_left()-dash_timeout.get_wait_time())/DASH_TIME_CONSTANT)
		velocity = dash_dir.normalized() * speed
	else:
		velocity = move_dir.normalized() * speed
	
	velocity = move_and_slide(velocity)

func get_sprite_direction():
	match move_dir:
		Vector2(-1,0):
			sprite_dir = "left"
		Vector2(1,0):
			sprite_dir = "right"
		Vector2(0,-1):
			sprite_dir = "up"
		Vector2(0,1):
			sprite_dir = "down"
		Vector2(-1,-1):
			sprite_dir = "upleft"
		Vector2(1,-1):
			sprite_dir = "upright"
		Vector2(-1,1):
			sprite_dir = "downleft"
		Vector2(1,1):
			sprite_dir = "downright"

func play_animation(animation):
	var new_animation = str(animation, sprite_dir)
	if animation_node.current_animation != new_animation:
		animation_node.play(new_animation)
		animation_node.seek(0)

func fire():
	if not reloading:
		var projectile = projectile_scene.instance()
		get_parent().add_child(projectile)
		projectile.init(position, (target - MOUSE_SCALE*position).angle())
		reloading = true
		reload_timeout.start()

func _on_ReloadTimeout_timeout():
	reloading = false

func dash():
	dash_ready = false
	dashing = true
	dash_timeout.start()
	dash_dir = move_dir
	speed = DASH_SPEED
	# do animation stuff
	# set invinsible

func _on_DashTimeout_timeout():
	speed = WALK_SPEED
	dashing = false
	# reset animation stuff
	# set not invinsible 
	dash_reload_timeout.start()

func _on_DashReload_timeout():
	dash_ready = true



