extends Node2D

onready var mob_scene = preload("res://Mob.tscn")

onready var spawn_pos = $SpawnPositions/SpawnPosition
onready var nav = $Navigation
onready var player = $Walls/Player
onready var walls = $Walls
onready var map_resource = load("res://Map.gd")
onready var floor_tile_map = $Navigation/Floor
onready var air_tile_map = $Air

export var map_width = 80
export var map_height = 80
enum {AIR, FLOOR, BRIDGE}  # the different tiles
 
func _ready():
	randomize()
	var map = map_resource.new(map_width, map_height)
	map.generate_map(map_width, map_height)
	place_map(map, floor_tile_map, air_tile_map)
	var player_start_position = air_tile_map.map_to_world(find_player_start_position(map, map_width, map_height))
	player.set_position(player_start_position)

func _physics_process(delta):
	pass

func _on_MobTimer_timeout():
	var mob = mob_scene.instance()
	walls.add_child(mob)
	mob.set_position(spawn_pos.get_position())
	mob.goal = player.get_position()
	mob.nav = nav

func place_map(map, floor_tile_map, air_tile_map):
	var floor_tile_set = floor_tile_map.get_tileset()
	var air_tile_set = air_tile_map.get_tileset()
	for x in range(map_width):
		for y in range(map_height):
			if map.map[x][y] == FLOOR:
				floor_tile_map.set_cell(x,y, floor_tile_set.find_tile_by_name("Floor"))
			elif map.map[x][y] == BRIDGE:
				floor_tile_map.set_cell(x,y, floor_tile_set.find_tile_by_name("Bridge"))
			elif map.map[x][y] == AIR:
				air_tile_map.set_cell(x - 1,y - 1, air_tile_set.find_tile_by_name("Air"))  # shift by one because of the "higher Z illusion"

#  Finds a valid start position for the player.
#  Uses a diagonal search from the top left tile.
func find_player_start_position(map, map_width, map_height):
	for i in range(map_width + map_height - 2):
		for x in range(i + 1):
			var y = i - x
			if y < map_height and x < map_width and map.map[x][y] != AIR:
				return Vector2(x,y)
	return Vector2(0,0)