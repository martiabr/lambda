extends Node2D

export var map_height = 80
export var map_width = 80

const MAX_ROOMS = 25

export var rect_room_max_size = 16
export var rect_room_min_size = 8
const RECT_ROOM_PROBABILITY = 0.1  # TODO: make probabilities in easier datastruct

const ROOM_HEIGHT_SPREAD_PARAMETER = 0.5

const NRECT_ROOM_MAX_SIZE = 24
const NRECT_ROOM_MIN_SIZE = 12
const NRECT_ROOM_SINGLE_MAX_SIZE = 10
const NRECT_ROOM_SINGLE_MIN_SIZE = 6
const NRECT_ROOM_SINGLE_PLACE_ATTEMPTS = 10
const TWO_RECT_ROOM_PROBABILITY = 0.075
const THREE_RECT_ROOM_PROBABILITY = 0.025
const FIVE_RECT_ROOM_PROBABILITY = 0.05

const CIRCLE_ROOM_MAX_RADIUS = 10
const CIRCLE_ROOM_MIN_RADIUS = 4
const CIRCLE_ROOM_PROBABILITY = 0.2

const TORUS_ROOM_MAX_INNER_RADIUS = 5
const TORUS_ROOM_MIN_INNER_RADIUS = 2
const TORUS_ROOM_MAX_OUTER_RADIUS = 14
const TORUS_ROOM_MIN_OUTER_RADIUS = 4
const TORUS_ROOM_MAX_WIDTH = 6
const TORUS_ROOM_MIN_WIDTH = 3
const TORUS_ROOM_PROBABILITY = 0.15

const CAVERN_MAX_SIZE = 28
const CAVERN_MIN_SIZE = 16
export var num_cavern_iterations = 14
export var cavern_initial_fill_probability = 0.5
export var birth_threshold = 3
export var starvation_threshold = 2
export var overpopulation_threshold = 7

export var shortcuts_enable = true
const PLACE_SHORTCUT_ATTEMPTS = 400
const MAX_SHORTCUT_LENGTH = 16
const MIN_SHORTCUT_PATHFINDING_DISTANCE = 30

const MAX_BRIDGE_LENGTH_INITIAL = 16
const MIN_BRIDGE_LENGTH_OFFSET = 3
const MAX_BRIDGE_LENGTH_OFFSET = 10

const MAX_BUILD_ROOM_ATTEMPTS = 400
const MAX_PLACE_ROOM_ATTEMPTS = 20
const MAX_FIND_BORDER_TILE_ATTEMPTS = 100

#  TODO: clean up export var vs const

enum {AIR, FLOOR, BRIDGE}  # the different tiles
const SEARCH_DIRECTIONS = PoolVector2Array([Vector2(-1, 0), Vector2(0,1), Vector2(1, 0), Vector2(0,-1)])

var rooms = []
var map = []

# TODO: find end position

func _ready():
	pass

func _init(width, height):
	map_width = width
	map_height = height

func generate_map(map_width, map_height):
	for x in range(map_width):  # fill map
		map.append([])
		for y in range(map_height):
			map[x].append(AIR)
	
	var room = generate_room()  # generate first room
	
	# Find top left tile to place first room in and place it:
	var room_dimensions = get_room_dimensions(room)
	var room_x = map_width / 2 - room_dimensions.x / 2
	var room_y = map_height / 2 - room_dimensions.y / 2
	place_room(room, room_x, room_y, map_width, map_height)
	
	# Generate and place other rooms:
	for i in range(MAX_BUILD_ROOM_ATTEMPTS):
		room = generate_room()
		var room_placement_data = find_room_placement(room, map_width, map_height)
		if not room_placement_data.empty():
			var room_placement = room_placement_data[0]
			var border_tile = room_placement_data[1]
			var direction = room_placement_data[2]
			var bridge_length = room_placement_data[3]
			
			place_room(room, room_placement.x, room_placement.y, map_width, map_height)
			
			place_bridge(border_tile, direction, bridge_length)
			
			if rooms.size() >= MAX_ROOMS:
				break
	
	if shortcuts_enable:
		place_shortcuts(map_width, map_height)

func generate_room():
	var room = []
	var choice = rand_range(0,1)

	if choice <= RECT_ROOM_PROBABILITY:  # decide between cavern or rectangle room
		room = generate_rect_room(0,0)
	elif choice <= RECT_ROOM_PROBABILITY + CIRCLE_ROOM_PROBABILITY:
		room = generate_circle_room(0)
	elif choice <= RECT_ROOM_PROBABILITY + CIRCLE_ROOM_PROBABILITY + TORUS_ROOM_PROBABILITY:
		room = generate_torus_room()
	elif choice <= RECT_ROOM_PROBABILITY + CIRCLE_ROOM_PROBABILITY + TORUS_ROOM_PROBABILITY + TWO_RECT_ROOM_PROBABILITY:
		room = generate_n_rect_room(2)
	elif choice <= RECT_ROOM_PROBABILITY + CIRCLE_ROOM_PROBABILITY + TORUS_ROOM_PROBABILITY + TWO_RECT_ROOM_PROBABILITY + THREE_RECT_ROOM_PROBABILITY:
		room = generate_n_rect_room(3)
	elif choice <= RECT_ROOM_PROBABILITY + CIRCLE_ROOM_PROBABILITY + TORUS_ROOM_PROBABILITY + TWO_RECT_ROOM_PROBABILITY + THREE_RECT_ROOM_PROBABILITY + FIVE_RECT_ROOM_PROBABILITY:
		room = generate_n_rect_room(5)
	else:
		room = generate_cavern()
	
	return room

# Generates a rectangular room with random height and width within max size bounds.
func generate_rect_room(room_width, room_height):
	if room_width == 0 or room_height == 0:
		#  Generate room width and height:
		room_width = randi_range(rect_room_min_size, rect_room_max_size)
		var min_room_height = max((room_width * ROOM_HEIGHT_SPREAD_PARAMETER) as int, rect_room_min_size)  # TODO: modularize this better
		var max_room_height = min((room_width * (1 + ROOM_HEIGHT_SPREAD_PARAMETER)) as int, rect_room_max_size)
		room_height = randi_range(min_room_height, max_room_height)
	
	var room = []
	for x in range(room_width):
		room.append([])
		for y in range(room_height):
			if x == 0 or x == room_width - 1 or y == 0 or y == room_height - 1:
				room[x].append(AIR)  # TODO: change this later with more blocks
			else:
				room[x].append(FLOOR)
	
	return room

func generate_n_rect_room(n):
	var room_dimensions = generate_room_dimensions(NRECT_ROOM_MIN_SIZE, NRECT_ROOM_MAX_SIZE, ROOM_HEIGHT_SPREAD_PARAMETER)	
	
	var room = generate_empty_room(room_dimensions.x, room_dimensions.y)
	
	var rect_rooms_placed = 0
	while rect_rooms_placed < n:
		var rect_room_dimensions = generate_room_dimensions(NRECT_ROOM_SINGLE_MIN_SIZE, NRECT_ROOM_SINGLE_MAX_SIZE, ROOM_HEIGHT_SPREAD_PARAMETER)
		var rect_room = generate_rect_room(rect_room_dimensions.x, rect_room_dimensions.y)
		
		if room_dimensions.x > rect_room_dimensions.x and room_dimensions.y > rect_room_dimensions.y:
			var rect_room_placement = Vector2(randi_range(1, room_dimensions.x - rect_room_dimensions.x), randi_range(1, room_dimensions.y - rect_room_dimensions.y))
			
			#  Check if rectangle overlap with previous:
			var overlap = false
			if rect_rooms_placed >= 1:
				for x in range(rect_room_dimensions.x):
					for y in range(rect_room_dimensions.y):
						if room[rect_room_placement.x + x][rect_room_placement.y + y] != AIR and rect_room[x][y] != AIR:
							overlap = true
							break
					if overlap:
						break
			
			if overlap or rect_rooms_placed == 0:  #  Place rectangle room in room if it overlaps previous or is the first
				for x in range(rect_room_dimensions.x):
					for y in range(rect_room_dimensions.y):
						if rect_room[x][y] != AIR:
							room[rect_room_placement.x + x][rect_room_placement.y + y] = rect_room[x][y]
				rect_rooms_placed += 1
	return room

func generate_circle_room(room_radius):
	if room_radius == 0:
		room_radius = randi_range(CIRCLE_ROOM_MIN_RADIUS, CIRCLE_ROOM_MAX_RADIUS)
	
	var room = generate_empty_room(2*(room_radius+1), 2*(room_radius+1))
	
	for x in range(-room_radius, room_radius+1):
		for y in range(-room_radius, room_radius + 1):
			if pow(x,2) + pow(y,2) <= pow(room_radius,2) + room_radius:
				room[x + room_radius + 1][y + room_radius + 1] = FLOOR
	return room

func generate_torus_room():
	var outer_radius = randi_range(TORUS_ROOM_MIN_OUTER_RADIUS, TORUS_ROOM_MAX_OUTER_RADIUS)
	var max_inner_radius = min(TORUS_ROOM_MAX_INNER_RADIUS, outer_radius - TORUS_ROOM_MIN_WIDTH)
	var min_inner_radius = min(max_inner_radius, max(TORUS_ROOM_MIN_INNER_RADIUS, outer_radius - TORUS_ROOM_MAX_WIDTH))
	var inner_radius = randi_range(min_inner_radius, max_inner_radius)
	var room = generate_circle_room(outer_radius)
	
	for x in range(-inner_radius, inner_radius+1):
		for y in range(-inner_radius, inner_radius + 1):
			if pow(x,2) + pow(y,2) <= pow(inner_radius,2) + inner_radius:
				room[x + outer_radius + 1][y + outer_radius + 1] = AIR
	return room

#  Generates a cavern inside a square of size CAVERN_MAX_SIZE using cellular automata.
#  By altering the number of iterations and the thresholds for overpopulation,
#  starvation and birth different results can be achieved.
#  - This is the slowest part of this entire class. If you want to optimize generation time, optimize this (probably flood_fill)
func generate_cavern():
	var dimensions = generate_room_dimensions(CAVERN_MIN_SIZE, CAVERN_MAX_SIZE, ROOM_HEIGHT_SPREAD_PARAMETER)
	var room = generate_empty_room(dimensions.x, dimensions.y)
	var empty_room = generate_empty_room(dimensions.x, dimensions.y)
	
	while room == empty_room:
		#  Random fill:
		for x in range(1, dimensions.x - 1):
			for y in range(1, dimensions.y - 1):
				if rand_range(0,1) <= cavern_initial_fill_probability:
					room[x][y] = FLOOR
		
		#  Do N iterations of cellular automata cavern generation 
		for i in range(num_cavern_iterations):
			var new_room = [] + room  # copy room
			for x in range(1, dimensions.x - 1):
				for y in range(1, dimensions.y - 1):
					var neighbours = count_neighbours(room, x, y)
					if room[x][y] == AIR:
						if neighbours >= birth_threshold:  # dead cell over birth limit becomes alive
							new_room[x][y] = FLOOR
					elif neighbours < starvation_threshold or neighbours > overpopulation_threshold:  # alive cell dies from starvation or overpopulation
						new_room[x][y] = AIR
			room = new_room
		room = flood_fill(room)
	
	return room

func flood_fill(room):
	var dimensions = get_room_dimensions(room)
	
	# do flood fill on all tiles. All processed tiles are set to AIR so none are done twice. Count size of area from flood fill, compare to global. Save largest set. Represent sets as vector2pools?
	var largest_set = PoolVector2Array()
	var largest_area = 0
	for x in range(1, dimensions.x - 1):
			for y in range(1, dimensions.y - 1):
				if room[x][y] != AIR:
					var queue = PoolVector2Array()
					queue.append(Vector2(x,y))
					var set = PoolVector2Array()
					var area = 0
					
					while queue.size() > 0:
						#  Remove tile from queue and increment area:
						var tile = queue[0]
						queue.remove(0)
						
						if room[tile.x][tile.y] != AIR:
							room[tile.x][tile.y] = AIR
							area += 1
							set.append(tile)
							
							#  Find nonzero neighbours and add to queue:
							for dir in SEARCH_DIRECTIONS:
								var new_tile = Vector2(tile.x,tile.y) + dir
								if room[new_tile.x][new_tile.y] != AIR:
									queue.append(new_tile)
					
					#  Compare to largest area found so far and update:
					if area > largest_area:
						largest_area = area
						largest_set = set
	#  Only fill largest area:
	var new_room = generate_empty_room(dimensions.x, dimensions.y)
	for tile in largest_set:
		new_room[tile.x][tile.y] = FLOOR
	
	return new_room

func place_room(room, room_x, room_y, map_width, map_height):
	var dimensions = get_room_dimensions(room)
	for x in range(dimensions.x):
		for y in range(dimensions.y):
			if room[x][y] != AIR and room_x + x < map_width and room_y + y < map_height:
				map[room_x + x][room_y + y] = room[x][y]
	rooms.append(room)

func find_room_placement(room, map_width, map_height):  # TODO: optimize and simplify this
	var room_dimensions = get_room_dimensions(room)
	
	for i in range(MAX_PLACE_ROOM_ATTEMPTS + 1):
		var direction = SEARCH_DIRECTIONS[randi_range(0, SEARCH_DIRECTIONS.size() - 1)]
		var border_tile = Vector2()
		var border_tile_found = false
		var j = 0
		while j <= MAX_FIND_BORDER_TILE_ATTEMPTS and not border_tile_found:
			var tile = Vector2()
			tile.x = randi_range(1, map_width - 2)
			tile.y = randi_range(1, map_height - 2)
			
			#  Check if random tile is air tile with air on one side and floor on other side:
			if map[tile.x][tile.y] == AIR and map[tile.x + direction.x][tile.y + direction.y] == AIR and map[tile.x - direction.x][tile.y - direction.y] != AIR:
				border_tile = tile
				border_tile_found = true
			j += 1
		if border_tile_found:
			#  Find random non air start tile in room:
			var start_tile = Vector2()
			var start_tile_found = false
			while not start_tile_found:
				start_tile.x = randi_range(0, room_dimensions.x - 1)
				start_tile.y = randi_range(0, room_dimensions.y - 1)
				if room[start_tile.x][start_tile.y] != AIR:
					start_tile_found = true
			start_tile = border_tile - start_tile  # Change from room coordinates to map coordinates
			
			#  Find the room placement as close as possible to the border tile without overlap:
			var bridge_length = 0
			var possible_room_placement = Vector2()
			for bridge_length in range (MAX_BRIDGE_LENGTH_INITIAL):  # Find valid initial placement
				possible_room_placement = start_tile + direction * bridge_length
				var overlap = check_overlap(room, possible_room_placement, map_width, map_height)
				if not overlap:
					break
			
			# Generate random bridge length offset within bounds, decrease until we have valid placement. Will terminate where we started with no offset
			var bridge_length_offset = randi_range(MIN_BRIDGE_LENGTH_OFFSET, MAX_BRIDGE_LENGTH_OFFSET)
			for bridge_length_with_offset in range(bridge_length_offset + bridge_length, bridge_length - 1, -1):
				possible_room_placement = start_tile + direction * bridge_length_with_offset
				var overlap = check_overlap(room, possible_room_placement, map_width, map_height)
				if not overlap:
					# TODO: reject room_placement and find new start_tile if bridge will hit several non air blocks with air between
					#var bridge_valid = check_bridge_valid(room, possible_room_placement, border_tile, direction, bridge_length)
					var bridge_valid = true
					var bridge_overlap = check_bridge_overlap(border_tile, direction, bridge_length_with_offset)
					if bridge_valid and not bridge_overlap:
						return [possible_room_placement, border_tile, direction, bridge_length_with_offset] 
			break  # Start tile did not give a valid placement, try next room placement
	return []  # failed to find room placement

func check_overlap(room, room_placement, map_width, map_height):  # TODO: optimize this
	var room_dimensions = get_room_dimensions(room)
	for x in range(room_dimensions.x):
		for y in range(room_dimensions.y):
			if room[x][y] != AIR:  # check that all 3x3 grid in map for all non air room tiles are air i.e. no overlap with map, with a guaranteed wall as well
				var map_x = x + room_placement.x
				var map_y = y + room_placement.y
				
				if map_x < 1 or map_x > map_width - 2 or map_y < 1 or map_y > map_height - 2:
					return true  # room placement is out of map bounds
				
				for neighbour_x in range(map_x - 1, map_x + 2):
					for neighbour_y in range(map_y - 1, map_y + 2):
						if map[neighbour_x][neighbour_y] != AIR:
							return true  # neighbour in map results in overlap
	return false

#  Checks that distance between tile and new room only has one gap.
func check_bridge_valid(room, room_placement, tile, direction, bridge_length):  # TODO: doesnt work, the room is not placed on map yet
	var num_gaps = 0
	var bridge_position = tile
	var last_tile_value =  map[bridge_position.x][bridge_position.y]
	for i in range(bridge_length):
		bridge_position += direction
		if last_tile_value != AIR and map[bridge_position.x][bridge_position.y] == AIR:
			num_gaps += 1
		last_tile_value = map[bridge_position.x][bridge_position.y]
	return num_gaps == 1

func check_bridge_overlap(tile, direction, bridge_length):
	var bridge_position = tile
	var normal_direction = Vector2()
	if direction.x == 0:
		normal_direction = Vector2(1,0)
	else:
		normal_direction = Vector2(0,1)
	
	# Move along possible bridge, check if all normal tiles are air if bridge tile is air 
	for i in range(bridge_length):
		bridge_position += direction
		if map[bridge_position.x][bridge_position.y] == AIR and (map[bridge_position.x + normal_direction.x][bridge_position.y + normal_direction.y] != AIR or map[bridge_position.x - normal_direction.x][bridge_position.y - normal_direction.y] != AIR):
			return true
	return false

func place_bridge(tile, direction, bridge_length):
	var bridge_position = tile
	map[bridge_position.x][bridge_position.y] = BRIDGE
	for i in range(bridge_length):
		bridge_position += direction
		if map[bridge_position.x][bridge_position.y] == AIR:
			map[bridge_position.x][bridge_position.y] = BRIDGE

func place_shortcuts(map_width, map_height):
	var graph = buildAStarGraph(map_width, map_height)
	
	# TODO: shuffle directions or just pick one. now it is partial to horizontal over vertical shortcuts. might not matter...
	for i in range(PLACE_SHORTCUT_ATTEMPTS + 1):
		var start_tile = Vector2(randi_range(1, map_width - 2), randi_range(1, map_width - 2))
		if map[start_tile.x][start_tile.y] != AIR:
			for direction in SEARCH_DIRECTIONS:
				if map[start_tile.x + direction.x][start_tile.y + direction.y] == AIR:
					#  Found valid start tile, try to move in direction and find new non air tile:
					for steps in range(2, MAX_SHORTCUT_LENGTH + 1):
						var x_end = start_tile.x + steps*direction.x
						var y_end = start_tile.y + steps*direction.y
						if not is_out_of_bounds(x_end, y_end, map_width, map_height):
							if map[x_end][y_end] != AIR:
								var distance = graph.get_point_path(start_tile.x + start_tile.y * map_width, x_end + y_end * map_width).size()
								
								if distance >= MIN_SHORTCUT_PATHFINDING_DISTANCE and not check_bridge_overlap(start_tile, direction, steps):  # found a valid shortcut
										place_bridge(start_tile + direction, direction, steps)
										graph.clear()
										graph = buildAStarGraph(map_width, map_height)
								else:
									break  # shortcut is not really a shortcut after all, try another direction
						else:
							break  # we are out of bounds, no point in continuing to increase bridge length

func buildAStarGraph(map_width, map_height):
	var graph = AStar.new()
	var directions = PoolVector2Array([Vector3(1, 0, 0), Vector3(0,1, 0)])
	
	# Add all tiles in graph:
	for x in range(map_width):
		for y in range(map_height):
			graph.add_point(x + y * map_width, Vector3(x, y, 0))  # ID is x + y * map_width such that each tile id is unique
	
	# Connect all tiles which have valid connections:
	for x in range(map_width - 1):
		for y in range(map_height - 1):
			for direction in directions:
				if map[x][y] != AIR and map[x + direction.x][y + direction.y] != AIR:
					graph.connect_points(x + y * map_width, (x + direction.x) + (y + direction.y) * map_width)  # connect_points() is by default bidirectional
	return graph

func is_out_of_bounds(x, y, map_width, map_height):
	return x < 0 or x >= map_width or y < 0 or y >= map_height

func generate_empty_room(width, height):
	var room = []
	for x in range(width):
		room.append([])
		for y in range(height):
			room[x].append(AIR)
	return room

func generate_room_dimensions(min_size, max_size, spread_parameter):
	var room_width = randi_range(min_size, max_size)
	var min_room_height = max((room_width * spread_parameter) as int, min_size)
	var max_room_height = min((room_width * (1 + spread_parameter)) as int, max_size)
	var room_height = randi_range(min_room_height, max_room_height)
	return Vector2(room_width, room_height)

func get_room_dimensions(room):
	return Vector2(room.size(), room[0].size())

func count_neighbours(room, x, y):
	var count = 0
	for i in range (x - 1, x + 2):
		for j in range (y - 1, y + 2):
			if room[i][j] != AIR and !(i != x and j != y):
				count += 1
	return count

func print_room(room):
	for x in range(room.size()):
		var row = ""
		for y in range(room[x].size()):
			row += str(room[x][y])
		print(row)

func randi_range(low, high):
	return randi() % (int(high) - int(low) + 1) + int(low)