extends Area2D

var velocity = Vector2()
export var speed = 400

func _ready():
	pass # Replace with function body.

func init(pos, rot):
	position = pos
	rotation = rot
	velocity = Vector2(speed, 0).rotated(rot)

func _physics_process(delta):
	position += velocity * delta


func _on_Projectile_body_entered(body):
	queue_free()
