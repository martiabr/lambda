extends KinematicBody2D

onready var collision_shape = $CollisionShape2D
onready var collision_offset = collision_shape.get_position()

export var health = 100.0
export var speed = 150
var velocity = Vector2()
var nav = null setget set_nav
var path = PoolVector2Array()
var goal = Vector2() 
var line = Line2D

func _ready():
	line = Line2D.new()
	line.set_default_color(Color.yellow)
	line.set_width(3)
	get_parent().add_child(line)
	
func set_nav(new_nav):
	nav = new_nav
	update_path()
 
func update_path():
	path = nav.get_simple_path(position + collision_offset, goal, false)
	line.set_points(path)
	
	if path.size() == 0:
		queue_free()

func _physics_process(delta):
	if path.size() >= 1:
		var d = (position + collision_offset).distance_to(path[0])
		if d > 15: # adjust this
			velocity = (path[0] - (position + collision_offset)).normalized() * speed
			velocity = move_and_slide(velocity)
		else:
			path.remove(0)

func _on_UpdatePathTimer_timeout():
	goal = get_parent().get_node("Player").get_position() + get_parent().get_node("Player/CollisionShape2D").get_position()
	update_path()